﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.Users.Profile.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();

                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "user-profile-filters")
        {
            CreateIndexIfNotExists("username_tenantId", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Username),true);
            CreateIndexIfNotExists("username", Builders<IFilterView>.IndexKeys.Ascending(i => i.Username));
            CreateIndexIfNotExists("isActive", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.IsActive));
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));
            view.TenantId = TenantService.Current.Id;

            Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == view.TenantId &&
                l.Username == view.Username;
           
            try
            {
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoCommandException)
            {
                //Retry one more time, if fail it will throw exception
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw exception
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            
        }

        public Task<IEnumerable<IFilterView>> GetAllActiveTeamMembers(string portal)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query.Where(p => (p.Permissions != null && p.Permissions.Contains(portal)) && p.IsActive)));
        }

        public Task<IEnumerable<IFilterView>> GetAllInactiveTeamMembers(string portal)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query.Where(p => (p.Permissions != null && p.Permissions.Contains(portal)) && !p.IsActive)));
        }

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.Name)
                            .AsQueryable();
        }
    }
}