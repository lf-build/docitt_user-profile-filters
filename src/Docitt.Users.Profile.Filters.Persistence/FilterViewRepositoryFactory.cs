﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Users.Profile.Filters.Persistence
{
    public class FilterViewRepositoryFactory : IFilterViewRepositoryFactory
    {
        public FilterViewRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            
            return new FilterViewRepository(tenantService, mongoConfiguration);
        }
    }
}