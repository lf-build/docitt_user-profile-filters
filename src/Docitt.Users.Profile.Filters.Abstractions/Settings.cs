﻿
using System;

namespace Docitt.Users.Profile.Filters
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "user-profile-filters";
    }
}