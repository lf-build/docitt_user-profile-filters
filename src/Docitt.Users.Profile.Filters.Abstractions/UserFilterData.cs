﻿namespace Docitt.Users.Profile.Filters
{
    public class UserFilterData
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public string Title { get; set; }
        public string Branch { get; set; }
        public string License { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}