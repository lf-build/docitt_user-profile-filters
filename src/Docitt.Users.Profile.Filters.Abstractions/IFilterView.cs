﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Users.Profile.Filters
{
    public interface IFilterView : IAggregate
    {
        string Username { get; set; }
        DateTimeOffset CreatedOn { get; set; }
        string Name { get; set; }
        string Title { get; set; }
        string Branch { get; set; }
        string License { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string CompanyId { get; set; } /*Added new field CompanyId*/
        bool IsActive { get; set; }
        IEnumerable<string> Roles { get; set; }

        IEnumerable<string> Permissions { get; set; }
        string BrandedUniqueCode { get; set; }
        string NmlsNumber { get; set; }
    }
}