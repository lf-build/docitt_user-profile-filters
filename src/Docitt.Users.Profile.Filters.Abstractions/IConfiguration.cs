﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Users.Profile.Filters
{
    public interface IConfiguration : IDependencyConfiguration
    {
         string ExportToCsvFileName { get; set; }
         EventMapping[] Events { get; set; }

         Dictionary<string, string> CsvProjection { get; set; }
    }
}