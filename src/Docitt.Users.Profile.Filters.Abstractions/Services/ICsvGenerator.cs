﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Users.Profile.Filters
{
    public interface ICsvGenerator
    {
        Task<byte[]> Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection);
    }
}