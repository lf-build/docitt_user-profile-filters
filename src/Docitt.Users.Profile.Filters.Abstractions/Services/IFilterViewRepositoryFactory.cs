﻿using LendFoundry.Security.Tokens;

namespace Docitt.Users.Profile.Filters
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}