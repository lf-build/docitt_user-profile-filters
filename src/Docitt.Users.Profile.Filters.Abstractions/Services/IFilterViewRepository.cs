﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Users.Profile.Filters
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        void AddOrUpdate(IFilterView view);

        Task<IEnumerable<IFilterView>> GetAllActiveTeamMembers(string portal);

        Task<IEnumerable<IFilterView>> GetAllInactiveTeamMembers(string portal);
    }
}