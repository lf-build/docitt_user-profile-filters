﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Users.Profile.Filters
{
    public interface IUserProfileFilterService
    {
        Task<IEnumerable<IFilterView>> GetAllActiveTeamMembers(string portal);

        Task<IEnumerable<IFilterView>> GetAllInactiveTeamMembers(string portal);

        Task<IUserProfileFileResult> GenerateAndExportToCsv(List<UserFilterData> userFilterData);
    }
}