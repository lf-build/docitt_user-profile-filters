﻿namespace Docitt.Users.Profile.Filters
{
    public interface IUserProfileFileResult
    {
        string FileName { get; set; }
        byte[] Content { get; set; }
    }
}