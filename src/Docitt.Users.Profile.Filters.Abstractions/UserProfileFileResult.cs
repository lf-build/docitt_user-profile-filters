﻿namespace Docitt.Users.Profile.Filters
{
    public class UserProfileFileResult : IUserProfileFileResult
    {
        public string FileName { get; set; }
        public byte[] Content { get; set; }
    }
}