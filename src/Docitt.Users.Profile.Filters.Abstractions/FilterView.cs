﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Users.Profile.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public string Username { get; set; }

        public string Name { get; set; }
        public string Title { get; set; }
        public string Branch { get; set; }
        public string License { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CompanyId { get; set; } /*Added new field CompanyId*/
        public DateTimeOffset CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<string> Roles { get; set; }

        public IEnumerable<string> Permissions { get; set; }
        public string BrandedUniqueCode { get; set; }
        public string NmlsNumber { get; set; }
    }
}