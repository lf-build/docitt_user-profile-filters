﻿namespace Docitt.Users.Profile.Filters
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string Username { get; set; }
    }
}