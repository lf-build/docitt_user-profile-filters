﻿using System.Collections.Generic;

namespace Docitt.Users.Profile.Filters
{
    public class Configuration : IConfiguration
    {
        public string ExportToCsvFileName { get; set; }
        public EventMapping[] Events { get; set; }

        public Dictionary<string, string> CsvProjection { get; set; }
        
        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
    }
}