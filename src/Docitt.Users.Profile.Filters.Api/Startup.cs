﻿using Docitt.UserProfile.Client;
using Docitt.Users.Profile.Filters.Docitt.Users.Profile.Filters;
using Docitt.Users.Profile.Filters.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Http;
#endif
using LendFoundry.Tenant.Client;

namespace Docitt.Users.Profile.Filters.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittUserProfileFilters"
                });
               c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
               {
                   Type = "apiKey",
                   Name = "Authorization",
                   Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                   In = "header"
               });
               c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.User.Profile.Filters.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#endif
            // services
           
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);

            services.AddIdentityService();

            services.AddUserProfileService();
            services.AddMongoConfiguration(Settings.ServiceName);
            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddTransient<IUserProfileFilterService, UserProfileFilterService>();
            services.AddTransient<IFilterViewRepository, FilterViewRepository>();
            services.AddTransient<IFilterViewRepositoryFactory, FilterViewRepositoryFactory>();
            services.AddTransient<ICsvGenerator, CsvGenerator>();
            services.AddTransient<IUserProfileFilterListener, UserProfileFilterListener>();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseUserProfileFilterListener();
            
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT User Profile filter Service");
            });
#endif   
            app.UseConfigurationCacheDependency();
        }
    }
}