﻿using Docitt.User.Profile.Filters.Api.ActionResults;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif
namespace Docitt.Users.Profile.Filters.Api.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service"></param>
        /// <exception cref="ArgumentException"></exception>
        public ApiController(IUserProfileFilterService service)
        {
            if (service == null) throw new ArgumentException($"{nameof(service)} is madatory");

            Service = service;
        }

        private IUserProfileFilterService Service { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portal"></param>
        /// <returns></returns>
        [HttpGet("/{portal}/all-active-members")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetAllActiveTeamMembers(string portal)
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllActiveTeamMembers(portal)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portal"></param>
        /// <returns></returns>
        [HttpGet("/{portal}/all-inactive-members")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetAllInactiveTeamMembers(string portal)
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllInactiveTeamMembers(portal)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFilterData"></param>
        /// <returns></returns>
        [HttpPost("/export-to-csv")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GenerateAndExportToCsv([FromBody]List<UserFilterData> userFilterData)
        {
            var fileResult = await Service.GenerateAndExportToCsv(userFilterData);
            return new FileActionResult(fileResult.FileName, fileResult.Content);
        }
    }
}