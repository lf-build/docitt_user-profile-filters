﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Docitt.Users.Profile.Filters
{
    public class CsvGenerator : ICsvGenerator
    {
        private Dictionary<string, string> CsvProjection { get; set; }

        public async Task<byte[]> Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection)
        {
            try
            {
                CsvProjection = csvProjection;
                return await Task.Run(() =>
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var streamWriter = new StreamWriter(memoryStream))
                        using (var csvWriter = new CsvWriter(streamWriter, SetCsvProjectionValues()))
                        {
                            csvWriter.WriteRecords(items);
                        } // StreamWriter gets flushed here.

                        return memoryStream.ToArray();
                    }
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CsvConfiguration SetCsvProjectionValues()
        {
            var csvConfiguration = new CsvConfiguration()
            {
                Encoding = Encoding.UTF8,
            };

            if (CsvProjection != null && CsvProjection.Any())
                csvConfiguration.RegisterClassMap(SetCsvClassMap());
            return csvConfiguration;
        }

        private CsvClassMap SetCsvClassMap()
        {
            var userFileMap = new DefaultCsvClassMap<UserFilterData>();
            CsvClassMap userCsvFileMap;
            Dictionary<string, string> csvProjection = CsvProjection;
            var columnIndex = 0;
            foreach (var csvKeyColumn in csvProjection)
            {
                var schemaPropertyName = csvKeyColumn.Key;
                var csvColumnName = csvKeyColumn.Value;
                if (!String.IsNullOrEmpty(csvColumnName))
                {
                    var propertyInfo = typeof(UserFilterData).GetProperty(schemaPropertyName);
                    var newMap = new CsvPropertyMap(propertyInfo);
                    newMap.Name(csvColumnName);
                    newMap.Index(columnIndex);
                    userFileMap.PropertyMaps.Add(newMap);
                    columnIndex += 1;
                }
            }

            userCsvFileMap = userFileMap;
            return userCsvFileMap;
        }
    }
}