﻿namespace Docitt.Users.Profile.Filters
{
    using LendFoundry.Foundation.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    namespace Docitt.Users.Profile.Filters
    {
        public class UserProfileFilterService : IUserProfileFilterService
        {
            public UserProfileFilterService
            (
                IFilterViewRepository repository,
                ILogger logger,
                Configuration configuration,
                ICsvGenerator csvGenerator
            )
            {
                Repository = repository ?? throw new ArgumentException($"{nameof(repository)} is mandatory");
                Logger = logger ?? throw new ArgumentException($"{nameof(logger)} is mandatory");
                Configuration = configuration ?? throw new ArgumentException($"{nameof(configuration)} is mandatory");
                CsvGenerator = csvGenerator;
            }

            private Configuration Configuration { get; }

            private IFilterViewRepository Repository { get; }

            private ILogger Logger { get; }

            private ICsvGenerator CsvGenerator { get; }

            public async Task<IEnumerable<IFilterView>> GetAllActiveTeamMembers(string portal)
            {
                return await Repository.GetAllActiveTeamMembers(portal);
            }

            public async Task<IEnumerable<IFilterView>> GetAllInactiveTeamMembers(string portal)
            {
                return await Repository.GetAllInactiveTeamMembers(portal);
            }

            public async Task<IUserProfileFileResult> GenerateAndExportToCsv(List<UserFilterData> userFilterData)
            {
                IUserProfileFileResult userProfileFileResult = null;
                if (userFilterData.Any())
                {
                    try
                    {
                        var responseContents = await CsvGenerator.Write(userFilterData, Configuration.CsvProjection);

                        userProfileFileResult = new UserProfileFileResult { FileName = Configuration.ExportToCsvFileName, Content = responseContents };
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error while generating csv file: ", ex);
                    }
                }
                return userProfileFileResult;
            }
        }
    }
}