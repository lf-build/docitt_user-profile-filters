﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
namespace Docitt.Users.Profile.Filters
{
    public static class UserProfileFilterListenerExtensions
    {
        public static void UseUserProfileFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IUserProfileFilterListener>().Start();
        }
    }
}