﻿using Docitt.UserProfile;
using Docitt.UserProfile.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Listener;
using System;
using System.Linq;
using LendFoundry.EventHub;
using System.Collections.Generic;

namespace Docitt.Users.Profile.Filters
{
    public class UserProfileFilterListener : ListenerBase, IUserProfileFilterListener
    {
        public UserProfileFilterListener
            (
                IConfigurationServiceFactory configurationFactory,
                ITokenHandler tokenHandler,
                IEventHubClientFactory eventHubFactory,
                IFilterViewRepositoryFactory repositoryFactory,
                ILoggerFactory loggerFactory,
                ITenantServiceFactory tenantServiceFactory,
                IIdentityServiceFactory identityServiceFactory,
                IUserProfileFactory userProfileFactory
            ): base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            EventHubFactory = eventHubFactory ?? throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");
            ConfigurationFactory = configurationFactory ?? throw new ArgumentException($"{nameof(configurationFactory)} is mandatory");
            TokenHandler = tokenHandler ?? throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");
            RepositoryFactory = repositoryFactory ?? throw new ArgumentException($"{nameof(repositoryFactory)} is mandatory");
            IdentityServiceFactory = identityServiceFactory ?? throw new ArgumentException($"{nameof(identityServiceFactory)} is mandatory");
            UserProfileFactory = userProfileFactory ?? throw new ArgumentException($"{nameof(userProfileFactory)} is mandatory");
        }

        private IIdentityServiceFactory IdentityServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private IUserProfileFactory UserProfileFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            return configuration?.Events.Select(x=>x.Name).Distinct().ToList();
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {
                var token = TokenHandler.Issue(tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var eventhub = EventHubFactory.Create(reader);
                var identityService = IdentityServiceFactory.Create(reader);
                var userProfileService = UserProfileFactory.Create(reader);
                var repository = RepositoryFactory.Create(reader);

                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    logger.Info($"#{configuration.Events.Count()} entity(ies) found from configuration: {Settings.ServiceName}");

                    var uniqueEvents = configuration.Events.Distinct().ToList();

                    uniqueEvents.ForEach(eventConfig =>
                    {
                        eventhub.On(eventConfig.Name, AddView(eventConfig, identityService, userProfileService, repository, logger));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");
                    });
                    return uniqueEvents.Select(x=>x.Name).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }
        }

        private static Action<EventInfo> AddView
        (
            EventMapping eventConfiguration,
            IIdentityService identityService,
            IUserProfileService userProfileService,
            IFilterViewRepository repository,
            ILogger logger
        )
        {
            return @event =>
            {
                try
                {
                    var username = eventConfiguration.Username.FormatWith(@event);

                    var user = identityService.GetUser(username).Result;

                    if (user != null)
                    {
                        var userProfile = userProfileService.GetProfile(username).Result;

                        // snapshot creation
                        var filterView = new FilterView();

                        filterView.Username = username;
                        filterView.IsActive = user.IsActive;
                        filterView.Roles = user.Roles;
                        filterView.Permissions = user.Permissions;
                        filterView.CreatedOn = user.CreatedOn.Value;
                        if (userProfile != null)
                        {
                            filterView.Name = $"{userProfile.FirstName} {userProfile.LastName}";
                            filterView.Title = userProfile.Title;
                            filterView.License = userProfile.License;
                            filterView.Email = userProfile.Email;
                            filterView.Phone = userProfile.Phone;
                            filterView.Branch = userProfile.BranchName;
                            filterView.CompanyId = userProfile.CompanyId; /*Added new field CompanyId*/
                            filterView.BrandedUniqueCode = userProfile.BrandedUniqueCode;
                            filterView.NmlsNumber = userProfile.NmlsNumber;
                        }

                        repository.AddOrUpdate(filterView);
                        logger.Info($"New snapshot added to user {username}");
                    }
                }
                catch (Exception ex) { logger.Error($"Unhadled exception while listening event {@event.Name}", ex, @event); }
            };
        }
    }
}