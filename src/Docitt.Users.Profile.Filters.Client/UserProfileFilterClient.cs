﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Users.Profile.Filters.Client
{
    public class UserProfileFilterClient : IUserProfileFilterService
    {
        public UserProfileFilterClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IEnumerable<IFilterView>> GetAllActiveTeamMembers(string portal)
        {
             return await Client.GetAsync<List<FilterView>>($"/{portal}/all-active-members");
        }

        public async Task<IEnumerable<IFilterView>> GetAllInactiveTeamMembers(string portal)
        {
            return await Client.GetAsync<List<FilterView>>($"/{portal}/all-inactive-members");
        }

        public async Task<IUserProfileFileResult> GenerateAndExportToCsv(List<UserFilterData> userFilterData)
        {
            return await Client.PostAsync<List<UserFilterData>, UserProfileFileResult>($"/export-to-csv", userFilterData, true);
        }
    }
}