﻿using LendFoundry.Security.Tokens;

namespace Docitt.Users.Profile.Filters.Client
{
    public interface IUserProfileFilterClientFactory
    {
        IUserProfileFilterService Create(ITokenReader reader);
    }
}