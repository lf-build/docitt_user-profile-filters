﻿using System;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Users.Profile.Filters.Client
{
    public static class UserProfileFilterClientExtensions
    {        
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddUserProfileFilters(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IUserProfileFilterClientFactory>(p => new UserProfileFilterClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IUserProfileFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddUserProfileFilters(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IUserProfileFilterClientFactory>(p => new UserProfileFilterClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IUserProfileFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddUserProfileFilters(this IServiceCollection services)
        {
            services.AddTransient<IUserProfileFilterClientFactory>(p => new UserProfileFilterClientFactory(p));
            services.AddTransient(p => p.GetService<IUserProfileFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        
    }
}