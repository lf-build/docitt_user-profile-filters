﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Users.Profile.Filters.Client
{
    internal static class UserProfileFilterClientServiceExtensions
    {
        public static IServiceCollection AddAppFlowService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IUserProfileFilterClientFactory>(p => new UserProfileFilterClientFactory(p));
            services.AddTransient(p => p.GetService<IUserProfileFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}